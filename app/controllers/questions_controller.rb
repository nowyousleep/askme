class QuestionsController < ApplicationController
  before_action :set_question, only: %i[update show destroy edit]
  def create
    question = Question.create(question_params)
    ## question_params = params.require(:question).permit(:body, :user_id)
    ## question = Question.create(question_params)
    #question = Question.create(
    #  body: params[:question][:body],
    #  user_id: params[:question][:user_id]
    #)
    #flash[:notice] = "Ваш новый вопрос создан!"
    redirect_to question_path(question), notice: "Новый вопрос создан!"
  end

  def update
    #@question = Question.find(params[:id]) - убрали из-за before_action
    @question.update(question_params)
    #@question.update( 
    #  body: params[:question][:body],
    #  user_id: params[:question][:user_id] 
    #) - убрали из-за question_params

    redirect_to question_path(@question), notice: "Сохранили вопрос!"
  end

  def destroy
    #@question = Question.find(params[:id])
    @question.destroy

    redirect_to questions_path, notice: "Удалили вопрос!"
  end

  def show
    #@question = Question.find(params[:id])
  end

  def index
    @question = Question.new
    @questions = Question.all
  end

  def new
    @question = Question.new
  end

  def edit
    #@question = Question.find(params[:id])
  end

  private

  def question_params
    params.require(:question).permit(:body, :user_id)
  end

  def set_question
    @question = Question.find(params[:id])
  end
end
